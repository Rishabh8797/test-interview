const express = require("express");
const routes = require("./routes/auth.route");
const { errorHandler } = require("./middlewares/error");
const ApiError = require("./utils/ApiError");
const helmet = require("helmet");
const cors = require("cors");
const compression = require("compression");
const multer = require('multer');


const app = express();

app.use(helmet());
app.use(express.json());

const fileStorage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, "images");
	},
	filename: (req, file, cb) => {
		cb(null, new Date().toISOString() + '-' + file.originalname)
	}
})



app.use(express.urlencoded({ extended: true }));
app.use(multer({ storage: fileStorage }).single('image'))

app.use(compression());

app.use(cors());
app.options("*", cors());


app.use("/v1", routes)


app.use((req, res, next) => {
	next(new ApiError(httpStatus.NOT_FOUND, "Not found"));
});

app.use(errorHandler)

module.exports = app;
