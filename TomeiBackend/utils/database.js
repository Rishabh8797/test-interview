const Sequelize = require("sequelize");
const config = require("../config/config");

const sequelize = new Sequelize('sign-backend', 'root', config.password, {
	dialect: "mysql",
	host: "localhost"
})

module.exports = sequelize;
