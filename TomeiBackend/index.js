const app = require("./app");
const config = require("./config/config");

const sequelize = require("./utils/database");

sequelize.sync().then(() => {
	console.log("Connected to DB")
	app.listen(config.port)
}).catch(err => {
	console.log(err)
})
