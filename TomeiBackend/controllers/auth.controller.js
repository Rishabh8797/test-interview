const { register } = require("../services/register.service");


const postRegister = async (req, res) => {
	try {
		const name = req.body.name;
		const email = req.body.email;
		const password = req.body.password;
		const image = req.file
		if (!image) {
			return res.status(400).send("Image file not proper")
		}
		const imageUrl = image.path;

		const result = await register(name, email, password, imageUrl);
		res.send(result)
	} catch (e) {
		res.status(400).send(e);
	}
}



module.exports = {
	postRegister,

}
