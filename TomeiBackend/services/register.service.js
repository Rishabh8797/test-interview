const { User } = require("../model/user.model")
const bcrypt = require("bcryptjs");

const register = async (name, email, password, imageUrl) => {
	try {
		const hashedPassword = await bcrypt.hash(password, 10);
		const user = await User.create({
			name: name,
			email: email,
			password: hashedPassword,
			imageUrl: imageUrl
		})
		return user;
	}
	catch (e) {
		throw e;
	}

};


module.exports = {
	register
}
