const dotenv = require("dotenv").config()
const path = require("path");


module.exports = {
	env: process.env.NODE_ENV,
	port: process.env.PORT,
	password: process.env.DB_PASSWORD
}
