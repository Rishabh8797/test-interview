const router = require("express").Router();
const { postRegister } = require("../controllers/auth.controller");


router.post("/register", postRegister);


module.exports = router;

